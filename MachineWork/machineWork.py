import sys
def readFile(filename):
    with open(filename, 'r') as f:
        cases = []
        company_info = f.readline().strip().split(' ')
        while company_info != ['0', '0', '0']:

            company_machines = CompanyMachines(*company_info)
            # print("Not of machine for sale = " + str(company_machines.N))
            for i in range(company_machines.N):
                machine_info = f.readline().strip().split(' ')
                # print(machine_info)
                company_machines.machines.append(Machine(*machine_info))
            cases.append(company_machines)
            company_info = f.readline().strip().split(' ')

    return cases


class Node(object):
    def __init__(self):
        self.buy = None
        self.stay = None

        self.curr_machine = None
        self.curr_dollars = 0

        self.node_day = None


class Machine(object):
    def __init__(self, di, pi, ri, gi):
        self.di = int(di)  # day for sale
        self.pi = int(pi)  # price
        self.ri = int(ri)  # resale value
        self.gi = int(gi)  # generated daily profit


class CompanyMachines(object):

    def __init__(self, N, C, D):
        self.N = int(N)  # number of machines
        self.C = int(C)  # company dollars
        self.D = int(D)  # days of restructure
        self.machines = []

    def maxProfit(self):
        for m_index in range(self.N):
            curr = m_index
            while curr > 0 and \
                    self.machines[curr - 1].di > \
                    self.machines[curr].di:
                self.machines[curr - 1], self.machines[curr] = \
                    self.machines[curr], self.machines[curr - 1]
                curr -= 1
        outcome_tree = Node()
        outcome_tree.curr_machine = None  # just to be  explicit
        outcome_tree.curr_dollars = self.C
        outcome_tree.node_day = 0


        curr_row = [outcome_tree, ]

        for machine in self.machines:
            next_row = []

            for outcome_tree in curr_row:

                outcome_tree.buy = Node()
                outcome_tree.stay = Node()
                outcome_tree.buy.node_day = machine.di
                outcome_tree.stay.node_day = machine.di


                sell_for = 0
                profit = 0
                if outcome_tree.curr_machine:
                    sell_for = outcome_tree.curr_machine.ri
                    profit = outcome_tree.curr_machine.gi * \
                                    (machine.di - outcome_tree.node_day - 1)

                if machine.pi <= (outcome_tree.curr_dollars + profit + sell_for):
                    outcome_tree.buy.curr_dollars = \
                                outcome_tree.curr_dollars + \
                                profit + \
                                sell_for + \
                                - machine.pi

                    outcome_tree.buy.curr_machine = machine

                    next_row.append(outcome_tree.buy)

                outcome_tree.stay.curr_machine = outcome_tree.curr_machine

                try:
                    outcome_tree.stay.curr_dollars = \
                            outcome_tree.curr_dollars + \
                            outcome_tree.curr_machine.gi * \
                            (machine.di - outcome_tree.node_day)
                except AttributeError:
                    outcome_tree.stay.curr_dollars = outcome_tree.curr_dollars
                next_row.append(outcome_tree.stay)

            curr_row = next_row

        max_outcome = 0
        for outcome in curr_row:
            try:
                final = outcome.curr_dollars + \
                        outcome.curr_machine.gi * \
                        (self.D - outcome.node_day) + \
                        outcome.curr_machine.ri
            except AttributeError:
                final = outcome.curr_dollars
            if final > max_outcome:
                max_outcome = final

        return max_outcome


if __name__ == '__main__':
    cases = readFile("input.txt")
    count = 1
    for case in cases:
        print('Case %i: %i' % (count, case.maxProfit()))
        count += 1